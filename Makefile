help:				## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

datasets = notebooks/datasets/data_london_gov_uk
app = jupyter-notebooks

dirs:				## Create directories
	mkdir -p $(datasets) figures

.geodata:			## Download geographical data
	cd $(datasets);\
	wget https://data.london.gov.uk/download/statistical-gis-boundary-files-london/b381c92b-9120-45c6-b97e-0f7adc567dd2/London-wards-2014.zip;\
	unzip London-wards-2014.zip

.elections:			## Download elections results
	cd $(datasets);\
	wget https://data.london.gov.uk/download/london-borough-profiles/c1693b82-68b1-44ee-beb2-3decf17dc1f8/london-borough-profiles.csv;\
	iconv -f ISO-8859-1 -t UTF-8//TRANSLIT london-borough-profiles.csv -o london-borough-profiles-utf8.csv

data: .geodata .elections	## Download datasets

setup:	dirs data		## Setup requirements

run-dev:			## run jupyter lab
	jupyter lab

build:				## build docker container
	docker build -t jupyternotebook -f config/jupyternotebook.Dockerfile .

run:				## run it via docker
	docker run -ti -v ${PWD}:/usr/local/bin/jupyternotebook -p 8888:8888 jupyternotebook

clean: .rm-images		## clean docker stuff

.ls-cntrs:			## list containers
	docker container ps -a | grep jupyternotebook

.rm-images:			## delete docker image
	docker image rm jupyternotebook
