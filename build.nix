with import <nixpkgs> {};

python3.withPackages (ps: with ps; [ cartopy geopandas ipython jupyter jupyterlab matplotlib numpy pandas seaborn toolz ])