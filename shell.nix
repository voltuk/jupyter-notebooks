with import <nixpkgs> {};

(
let
  in python37.withPackages (ps: with ps; [ geopandas ipython jupyter jupyterlab matplotlib numpy pandas seaborn toolz ])
).env
