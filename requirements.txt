geopandas==0.5.1
ipython==7.6.1
jupyter==1.0.0
jupyterlab==0.35.6
matplotlib==3.1.1
numpy==1.17.2
pandas==0.25.1
seaborn==0.9.0
toolz==0.10.0

